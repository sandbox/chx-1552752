<?php

/**
 * @file
 * Contains the select style plugin.
 */

/**
 * Style plugin to render items in a select box.
 *
 * @ingroup views_style_plugins
 */
class fieldapi_views_widget_plugin_style_widget_select extends views_plugin_style {
  function option_definition() {
    $options = parent::option_definition();

    $options['key'] = array('default' => '');

    return $options;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $handlers = $this->display->handler->get_handlers('field');
    if (!empty($handlers)) {
      $form['markup'] = array(
        '#markup' => t('You can pick a field that will be used as the value of the option. You should then set that field to exclude. All other displayed fields will be displayed as the human readable option. Please note that all HTML will be stripped from this output as select boxes cannot show HTML.'),
        '#prefix' => '<div class="form-item description">',
        '#suffix' => '</div>',
      );

      foreach ($handlers as $id => $handler) {
        $options[$id] = $handler->ui_name();
      }

      $form['key'] = array(
        '#type' => 'select',
        '#title' => t('Key field'),
        '#options' => $options,
        '#default_value' => $this->options['key'],
      );
    }
    else {
      $form['error_markup'] = array(
        '#markup' => t('You need at least one field before you can configure your views widget settings'),
        '#prefix' => '<div class="error messages">',
        '#suffix' => '</div>',
      );
      return;
    }
  }

  function render() {
    $sets = $this->render_grouping($this->view->result, $this->options['grouping']);

    // Turn this all into an $options array for the jump menu.
    $this->view->row_index = 0;
    $options = array();
    $key_field = $this->options['key'];

    foreach ($sets as $title => $records) {
      foreach ($records as $row_index => $row) {
        $this->view->row_index = $row_index;
        $key = strip_tags(decode_entities($this->get_field($this->view->row_index, $key_field)));
        $label = strip_tags(decode_entities($this->row_plugin->render($row)));
        $options[$key] = $label;
        $this->view->row_index++;
      }
    }
    unset($this->view->row_index);
    return $options;
  }
}
